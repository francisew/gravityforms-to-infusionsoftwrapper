function getMapped( data ) {
  if(data.match(/First/) && data.match(/Name/))
    return "FirstName"
  if(data.match(/Sur/) && data.match(/name/))
    return "LastName"
  if(data.match(/Email/))
    return "Email"
  if(data.match(/Phone/))
    return "Phone1"
  if(data.match(/State/))
    return "State"
  if(data.match(/Interested In/))
    return "_InterestedIn"
  return data.replace('*', '')
}
function submitToInfusionsoftWrapper(e) {
    e.preventDefault();
    jQuery(e.target).attr('disabled', true)
    let fields = []
    jQuery(e.target.form).find('input:not([type="submit"]):not([type="hidden"]), select').each((key, val) => {
      const fieldName = getMapped( jQuery(val).closest('li').find('label')[0].textContent )
      fields[ fieldName ] = val.value
    }).promise().done( function(){ 
      jQuery.ajax({
          type : "post",
          dataType : "json",
          url : sbiajaxurl || et_pb_custom.ajaxurl, // using the site's ajaxurl variable 
          data : {
            action: "toinfusionsoft_wrapper",
            ...fields,
          },
          success: function(response) {
            if(!isNaN(parseInt(response))) {
              alert('Successfuly Registered!');
              window.location.href= "/thank-you-appointment/";
            }
            else {
              alert('Something went wrong.')
              console.log(response);
              jQuery(e.target).attr('disabled', false)
            }
          }
      })
    });
    return false;
}
jQuery(document).ready(function($) {

  $('input[aria-required], select[aria-required]').each(function() {
    $(this).prop('required',true)
  })
  $('input').blur(function(event) {
      event.target.checkValidity();
  }).bind('invalid', function(event) {
    alert('Please fill up required fields.')
      setTimeout(function() { $(event.target).focus();}, 50);
    $(this).unbind('invalid')
  });
  function setAttributes(el, attrs) {
      for(var key in attrs) {
          el.setAttribute(key, attrs[key]);
      }
  }

  const new_submit_button = document.createElement('button');
  setAttributes(new_submit_button, {
      class : 'gform_button button',
      onClick : 'submitToInfusionsoftWrapper(event)'
  })
  new_submit_button.innerHTML = $('.gform_footer input[type="submit"]').attr('value');
$('.gform_footer').append(new_submit_button);

});